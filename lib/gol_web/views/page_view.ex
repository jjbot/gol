defmodule GolWeb.PageView do
  use GolWeb, :view

  def get_cell_state_class(board, loc) do
    "square-#{board[loc]}"
  end
end
