defmodule GolWeb.PageLive do
  use GolWeb, :live_view

  @impl true
  def render(assigns) do
    Phoenix.View.render(GolWeb.PageView, "page_live.html", assigns)
  end

  @impl true
  def mount(_params, _session, socket) do
    if connected?(socket), do: :timer.send_interval(5000, self(), :tick)

    board = display_board(Board.get_board())
    {:ok, assign(socket, board: board, player: {0, 0})}
  end

  @impl true
  def handle_event("keydown", %{"key" => " "}, socket) do
    board =
      Board.flip(socket.assigns.player)
      |> display_board()
    {:noreply, assign(socket, :board, board)}
  end

  def handle_event("keydown", %{"key" => key}, socket) do
    position = socket.assigns.player
    player = move(position, key)
    IO.puts(inspect player)
    {:noreply, assign(socket, :player, player)}
  end

  @impl true
  def handle_info(:tick, socket) do
    board = display_board(Board.get_board())
    {:noreply, assign(socket, :board, board)}
  end

  def move({px, py}, "ArrowUp"), do: {rem(px + 9, 10), py}
  def move({px, py}, "ArrowDown"), do: {rem(px + 1, 10), py}
  def move({px, py}, "ArrowLeft"), do: {px, rem(py + 9, 10)}
  def move({px, py}, "ArrowRight"), do: {px, rem(py + 1, 10)}
  def move(loc, key) do
    IO.puts(key)
    loc
  end

  def display_board(board) do
    board
    |> Enum.map(
      fn {pos, val} -> 
        if val == 1 do
          {pos, "on"}
        else
          {pos, "off"}
        end
      end)
    |> Enum.reduce(%{}, fn {pos, val}, acc -> Map.put(acc, pos, val) end)

  end
end
