defmodule Board do
  use GenServer

  def flip(pos), do: GenServer.call(__MODULE__, {:flip, pos})

  def get_board(), do: GenServer.call(__MODULE__, :get_board)

  @impl GenServer
  def init(_) do
    coordinates = for x <- 0..9, y <- 0..9, do: {x, y}
    board = Enum.reduce(
      coordinates,
      %{},
      fn {x, y}, acc ->
        status =
          if rem(x + y, 2) == 0 do
            1
          else
            0
          end
        Map.put(acc, {x, y}, status)
      end
    )

    :timer.send_interval(5000, self(), :tick)
    {:ok, board}
  end

  def start_link(_) do
    GenServer.start_link(__MODULE__, nil, name: __MODULE__)
  end

  @impl GenServer
  def handle_call({:flip, pos}, _from, board) do
    board = Map.update!(board, pos, fn existing -> rem(existing + 1, 2) end)
    IO.puts("Flipped to: #{inspect Map.get(board, pos)}")
    {:reply, board, board}
  end

  def handle_call(:get_board, _from, board) do
    {:reply, board, board}
  end

  @impl GenServer
  def handle_info(:tick, board) do
    coordinates = for x <- 0..9, y <- 0..9, do: {x, y}
    new_board =
      Enum.reduce(
        coordinates,
        %{},
        fn loc, acc ->
          Map.put(acc, loc, get_status(board, loc))
        end
      )

    if board == new_board do
      {:noreply, generate_random_board()}
    else
      {:noreply, new_board}
    end
  end

  def generate_random_board() do
    coordinates = for x <- 0..9, y <- 0..9, do: {x, y}

    Enum.reduce(
      coordinates,
      %{},
      fn c, acc ->
        if :random.uniform() > 0.5 do
          Map.put(acc, c, 1)
        else
          Map.put(acc, c, 0)
        end
      end
    )
  end

  def get_status(board, loc) do
    active_count = 
      get_neighbours(loc)
      |> Enum.map(&Map.get(board, &1, 0))
      |> Enum.sum()

    case {Map.get(board, loc), active_count} do
      {0, 3} -> 1
      {1, active_count} when active_count == 2 or active_count == 3 -> 1
      {_, _} -> 0
    end

  end

  def get_neighbours({cx, cy}) do
    positions =
      for dx <- [-1, 0, 1], dy <- [-1, 0, 1] do
        {cx + dx, cy + dy}
      end
    Enum.reject(positions, fn {x, y} -> x == cx and y == cy end)
  end
end
