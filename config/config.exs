# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
config :gol, GolWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "6azGtPja1iv5jDks3f7GfCno1L0DbPyu6VrQEJogh5woQ3EnXPy9Vt+Rmwsa3m7m",
  render_errors: [view: GolWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Gol.PubSub,
  live_view: [signing_salt: "th2v1AIX"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
